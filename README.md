#Repositório da Página do GIMM e Repositórios


Seja bem vindo ao repositório da página oficial do GIMM - Grupo de Informática Médica e Microeletrônica
do curso de Engenharia de Computação da Universidade Federal do Pampa.

Julio Saraçol



#### Repositórios do Grupo:
As diferentes soluções do grupo GIMM estão acessíveis através dos repositórios abaixo:

 
## Repositórios

<img src="img/logoGIM.jpg" align="center" />

**ARFIsio:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)

**Victus:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)

**Victus-VR:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)

**Pysiopong:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)

**FootInspect:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)

**MFID:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)

**Tennis Fisio:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)

**Fisio Lung:**  <br>
[GitLab](https://gitlab.com/gimm.unipampa/page/-/blob/master/README.md)


## Informações da Página do Grupo:

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:3.7-alpine

test:
  stage: test
  script:
  - pip install -U sphinx sphinx_rtd_theme
  - sphinx-build -b html . public
  only:
  - branches
  except:
  - master

pages:
  stage: deploy
  script:
  - pip install -U sphinx sphinx_rtd_theme
  - sphinx-build -b html . public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Requirements

- [Sphinx][]

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][sphinx] Sphinx
1. Generate the documentation: `make`

The generated HTML will be located in the location specified by `conf.py`,
in this case `_build/html`.

