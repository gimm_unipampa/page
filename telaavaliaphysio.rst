.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Avalia Physio
================
	
Este projeto busca desenvolver um sistema multiplataforma para auxiliar os profissionais de fisioterapia na avaliação funcional de pacientes internados na UTI através de um software que analisa a execução dos seis movimentos específicos, os quais envolvem os membros inferiores e superiores do corpo humano. Os pacientes que passarão pela avaliação funcional deverão estar cadastrados no sistema, onde devem ser registradas pelo fisioterapeuta informações pessoais para sua identificação. O profissional de fisioterapia também deverá estar cadastrado no sistema com as suas principais informações para registro do mesmo em cada avaliação realizada. O sistema possuirá um registro de cada avaliação, onde será informado o paciente e o fisioterapeuta, contendo informações sobre o movimento de cada um dos membros para que seja possível acompanhar a evolução do paciente pelo sistema. Relatórios de pacientes poderão ser gerados para acompanhamento mais preciso das avaliações realizadas ao decorrer do tempo.


.. image:: img/avaliaphysio/arqsistema.jpeg
.. image:: img/avaliaphysio/tela-inicial.jpg
.. image:: img/avaliaphysio/tela-mrc.jpg
.. image:: img/avaliaphysio/tela-mrc-3.jpg
.. image:: img/avaliaphysio/tela-mrc-res.jpg



Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png

