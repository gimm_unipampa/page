.. image:: img/bannergimm.png

Publicações
**************


Artigos 
=================================

* 2016
======

ARRIEIRA, M. S. R. ; SOARES, M. ; VIEIRA, M. ; BARANANO, R. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . A Computação Como Instrumento De Apoio Na Reabilitação Motora De Pacientes Amputados De Membro Inferior. In: VIII Congresso Sul Brasileiro de Computação (SULCOMP), 2016, Criciúma - SC. VIII Congresso Sul Brasileiro de Computação (SULCOMP), 2016. v. 8.

* 2017
======

ARRIEIRA, M. S. R. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Sistema Computacional Como Ferramenta De Apoio Ao Tratamento De Indivíduos Amputados. REVISTA DO CCEI, v. 22, p. 82-96, 2017.

ARRIEIRA, M. S. R. ; CORREA, D. ; VIEIRA, M. ; BARANANO, R. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Computação Aplicada no Monitoramento da Reabilitação Física em Indivíduos Amputados. In: Computer on the Beach 2017, 2017, Florianópolis - SC. Computer on the Beach, 2017.

FOLLMANN, J. ; COLLAR, P. T. ; RIBEIRO, G. B. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, J. S. . Realidade Aumentada Aplicada ao Processo de Reabilitação Física de Membro Superior. In: XXXVII Congresso Da Sociedade Brasileira De Computação (WIM), 2017, São Paulo. XXXVII Congresso Da Sociedade Brasileira De Computação, 2017. p. 2072-2075.

RIBEIRO, G. B. ; COLLAR, P. T. ; FOLLMANN, J. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Reabilitação Física de Membro Superior: Uma proposta baseada em Realidade Aumentada. In: 19th SYMPOSIUM ON VIRTUAL AND AUGMENTED REALITY, 2017, Curitiba - PR. 19th SYMPOSIUM ON VIRTUAL AND AUGMENTED REALITY, 2017.


ARRIEIRA, M. S. R. ; CORREA, D. ; VIEIRA, M. ; BARANANO, R. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Avaliação do Processo de Reabilitação Física Em Indivíduos Amputados de Membros Inferiores a Partir da Geração Automatizada de Indicadores de Desempenho. In: Congrega Urcamp 2017, 2017, Bagé. Congrega Urcamp 2017, 2017.

FOLLMANN, J. ; LADEIA, G. ; COLLAR, P. T. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Arfisio: Uma Proposta Para Reabilitação Física De Membro Superior Utilizando A Realidade Aumentada. In: Congrega Urcamp 2017, 2017, Bagé. Congrega Urcamp 2017, 2017.


COLLAR, P. T. ; RIBEIRO, G. B. ; BARANANO, R. ; VIEIRA, M. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Baropodometria: Sistema In-Shoe. In: Congrega Urcamp 2017, 2017, Bagé-RS. Congrega Urcamp 2017, 2017.

PANDOLFO, E. ; DOMINGUES JÚNIOR, J. S. . Proposta De Um Módulo Para Descrições Verilog Na Ferramenta Flexmap. In: 9º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2017, Livramento-RS. 9º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2017.

FOLLMANN, J. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . AR Fisio: Realidade Aumentada aplicada ao processo de reabilitação física de amputados de membro superior. In: 9º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2017, Livramento-RS. 9º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2017.

* 2018
======

ARRIEIRA, M. S. R. ; CORREA, D. ; VIEIRA, M. ; BARANANO, R. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Desenvolvimento de Uma Solução Computacional Aplicada no Acompanhamento do Processo de Reabilitação Física em Pacientes Amputados de Membros Inferiores. In: Computer on the Beach 2018, 2018, Florianópolis - SC. Computer on the Beach 2018, 2018.


LADEIA, G. ; PAIVA, B. ; ARRIEIRA, M. S. R. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Creation of Games for Physical Rehabilitation of Amputees Using an Adapted Flow Model as a Basis. In: XVII SBGames, 2018, Foz do Iguaçu-PR. XVII SBGames, 2018.


COLLAR, P. T. ; MEIRELLES, D. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . Baropodômetro: Sistema de Palmilhas Sensíveis a Pressão. In: Computer on the Beach 2018, 2018, Florianópolis - SC. Computer on the Beach 2018, 2018.



* 2019
======

PAIVA, B. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, J. S. . Desenvolvimento de um sistema para identificação da fadiga muscular durante o processo de reabilitação física de pacientes amputados. In: Computer On The Beach 2019, 2019, Florianopolis-SC. Computer On The Beach 2019, 2019.

* 2020
======

PAIVA, B. ; FOLLMANN, J. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, J. S. . MFID: Uma solução computacional para a identificação da fadiga muscular durante a reabilitação física de indivíduos amputados. In: Computer On The Beach 2020, 2020, Balneário Camboriu-SC. Computer On The Beach 2019, 2020.

FOLLMANN, J. ; PAIVA, B. ; DOMINGUES JÚNIOR, J. S. ; AMARAL, E. M. H. . PhysioPong: Serious Game Aplicado ao Processo de Reabilitação Física de Amputados de Membro Superior. In: Computer On The Beach 2020, 2020, Balneário Camboriu-SC. Computer On The Beach 2020, 2020.

DOMINGUES, JULIO SARACOL; DA ROSA, LEOMAR SOARES ; DE SOUZA MARQUES, FELIPE . A Straightforward Methodology for QCA Circuits Design. In: 2020 33rd Symposium on Integrated Circuits and Systems Design (SBCCI), 2020, Campinas. 2020 33rd Symposium on Integrated Circuits and Systems Design (SBCCI), 2020. p. 1.


* 2021
=======

PINHEIRO, A. N. L.; VIEIRA, M. B.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. TennisGame Physio: Proposta de Solução no Apoio de Sessões de Fisioterapia para Amputados In: XX Brazilian Symposium on Computer Games and Digital Entertainment - SBGAMES, 2021, Gramado - RS. XX SBGAMES. , 2021.
  
JUNIOR, JULIO SARACOL DOMINGUES; DA ROSA JUNIOR, LEOMAR SOARES; DE SOUZA MARQUES, FELIPE. Migortho: A DESIGN AUTOMATION FLOW FOR QCA CIRCUITS. IEEE Design & Test., v.39, p.1 - 1, 2021.


LEON, G. O. M.; DOMINGUES JÚNIOR, J. S. Ausculsensor: Ausculta Automática Para Fisioterapia Respiratória In: 13º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2021, Bagé. 13º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2021. v.13.

SEGUI, R. A.; DOMINGUES JÚNIOR, J. S. Botpampa: Proposta De Desenvolvimento De Um Chatbot Para Universidade Federal Do Pampa (Unipampa) In: 13º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2021, Bagé. 13º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2021. v.13.

PINHEIRO, A. N. L.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. Tennisgame Physio: Proposta De Solução No Apoio De Sessões De Fisioterapia Para Amputados In: 13º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2021, Bagé. 13º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2021. v.13.
  
* 2022
=======

LEON, G. O. M. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, J. S. . Ausculsensor: Uma Exploração no Espaço de Projeto para Ausculta Pulmonar Automática na Fisioterapia Respiratória. In: Computer On The Beach 2022, 2022, Itajaí-SC. Computer On The Beach 2022, 2022.

LEON, G. O. M. ; JARDIM, A. T. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, J. S. . Fisioterapia Respiratória: Proposta de um sistema para o monitoramento de sessões. In: Simpósio Brasileiro de Computação Aplicada à Saúde (SBCAS), 2022, Teresina - PI. XXII Simpósio Brasileiro de Computação Aplicada à Saúde 2022 (SBCAS2022), 2022.

LEON, G. O. M. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, J. S. . Ausculsensor: Uma exploração no Espaço de Projeto para Ausculta Pulmonar Automática na Fisioterapia Respiratória. In: Simpósio Brasileiro de Computação Aplicada à Saúde (SBCAS), 2022, Teresina - PI. XXII Simpósio Brasileiro de Computação Aplicada à Saúde 2022 (SBCAS2022), 2022.


PINHEIRO, A. N. L.; VIEIRA, M. B.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. Tennisgame Physio: A tool for amputees physiotherapy based on gamification. Journal on Interactive Systems (JIS). , v.13, p.301 - 312, 2022.


LEON, G. O. M.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. Sistema para auxílio na aplicação e análise da Ausculta Pulmonar In: XIX Congresso Brasileiro de Informática em Saúde (CBIS), 2022, Campinas - SP. XIX Congresso Brasileiro de Informática em Saúde (CBIS2022)., 2022.


* 2023
=======

LEON, G. O. M.; DOMINGUES, W. S.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. FisioLung: A System for Training and Monitoring of Respiratory Physiotherapy Sessions. Journal on Interactive Systems (JIS). , v.14, p.000 - 000, 2023.


LEON, GIULIANA OLIVEIRA DE MATTOS; AMARAL, ÉRICO MARCELO HOFF DO; DOMINGUES JÚNIOR, JULIO SARAÇOL. Sistema para auxílio na aplicação e análise da Ausculta Pulmonar. journal of health informatics. , v.15, p.1-14 - , 2023.


MENDES, D. A. T.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. Physio Games – Ambiente de Integração de Jogos Sérios para Reabilitação Física de Amputados In: Simpósio Brasileiro de Computação Aplicada à Saúde (SBCAS), 2023, Campinas. Simpósio Brasileiro de Computação Aplicada à Saúde 2023 (SBCAS2023). , 2023.


MENDES, D. A. T. ; MELO, R. L. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, JULIO SARAÇOL . Proposta de um Ambiente Integrado para Reabilitação Física de Amputados Utilizando Jogos Sérios. In: XVII Brazilian Symposium on Computer Games and Digital Entertainment (SBGAMES), 2023, Rio Grande. XXII SBGAMES, 2023.


MELO, R. L. ; MENDES, D. A. T. ; AMARAL, E. M. H. ; DOMINGUES JÚNIOR, JULIO SARAÇOL . Gamificação na Reabilitação Física de Amputados: Uma Abordagem Baseada em Jogo Sério. In: XVII Brazilian Symposium on Computer Games and Digital Entertainment (SBGAMES), 2023, Rio Grande. XXII SBGAMES, 2023. [Terceiro colocado Best Paper na tracking Saúde, Simpósio Brasileiro de Jogos e Entretenimento Digital (SBGAMES 2023)].


MIRANDA, L. G.; MENDES, D. A. T.; JUSTI, G. H.; DOMINGUES JÚNIOR, J. S.; JUSTI, A. C. A. Construção De Um Equipamento De Baixo Custo Para Amostragem De Material Particulado In: 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2023, Caçapava do Sul.  15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2023. v.3.


DOMINGUES, W. S.; DOMINGUES JÚNIOR, J. S.; PINHO, L. B. Movebus: Uma Solução Computacional Móvel Escalável Baseada Em Pwa Para Eficientização Do Transporte Urbano In: 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2023, Caçapava do Sul. 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2023. v.3.


MENDES, D. A. T.; MIRANDA, L. G.; DOMINGUES JÚNIOR, J. S.; JUSTI, A. C. A.; JUSTI, G. H. Projeto E Construção De Uma Estação Meteorológica In: 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2023, Caçapava do Sul.  15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2023. v.3.


FERREIRA, V.; CORREA, D. T.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. Proposta De Um Jogo Sério Para Fisioterapia Respiratória In: 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2023, Caçapava do Sul. 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2023. v.3.


JARDIM, T.; CORREA, D.; AMARAL, E. M. H.; DOMINGUES JÚNIOR, J. S. Respiroscan: Sistema Integrado De Avaliação E Treinamento Respiratório In: 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE, 2023, Caçapava do Sul. 15º Salão Internacional de Ensino, Pesquisa e Extensão - SIEPE. , 2023. v.3.


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
