
.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Ausculsensor
============

o presente trabalho tem como objetivo desenvolver um sistema hábil para realização da etapa de ausculta pulmonar de forma automatizada, por meio da integração de hardware e software, sendo um nó sensor acoplado a um este-
toscópio, estruturado para capturar a frequência durante a etapa de ausculta pulmonar. Foi realizado uma exploração no espaço de projeto para a implementação do nó sensor considerando diferentes critérios, dentre eles, cita-se baixo custo versus qualidade do sinal obtido para a realização da ausculta pulmonar. Outro viés da ferramenta, é o auxilio no treinamento de estudantes da área da saúde, além da análise da ausculta através de técnicas de machine learning. Esta solução visa auxiliar os profissionais da área da saúde a obter os resultados mais rápidos e de forma automatizada, além de auxiliar os estudantes em sua aprendizagem.


.. image:: img/asculsensor/arquitetura.png
.. image:: img/asculsensor/app.png
.. image:: img/asculsensor/app2.png
.. image:: img/asculsensor/asculsensor.png

Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png


