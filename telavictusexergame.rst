.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Victus-Exergames
=========

Este trabalho apresenta uma solução projetada para aprimorar o processo de reabilitação física de pessoas com amputações, utilizando uma ferramenta baseada em informática médica combinada com elementos de gamificação por meio de um jogo sério. Abordando os desafios enfrentados por indivíduos com amputações de membros inferiores durante a fisioterapia - como trauma, dor e falta de motivação - este trabalho introduz um jogo sério que integra um sistema de sensores embutido com microcontroladores a uma bicicleta estacionária. Esse sistema serve tanto como controlador do jogo, quanto como um conjunto de monitores biológicos, além de ser uma ferramenta de fisioterapia que exibe os dados obtidos durante as sessões, permitindo que os terapeutas acompanhem o progresso do paciente. Desenvolvido mediante uma abordagem de design participativo envolvendo pacientes e terapeutas, o sistema coleta dados sobre o envolvimento do usuário, respostas fisiológicas e métricas de desempenho por meio de sensores. O desenvolvimento do sistema se inicia com uma pesquisa bibliográfica inicial para fundamentar o projeto, seguida pela organização dos componentes do sistema em uma aplicação unificada. A fase de prototipação e testes iniciais focou na integração dos sensores de baixo custo na bicicleta ergométrica utilizada no SRF, resultando em conclusões prévias, especialmente acerca dos incrementos no jogo sério realizados. Melhorias na gamificação e na conectividade dos sensores foram implementadas para aumentar a imersão e praticidade no uso do sistema, como a criação de desafios e novos ambientes virtuais no jogo. Testes de usabilidade foram conduzidos para refinar a experiência do usuário, e a etapa seguinte no desenvolvimento consistirá na implementação da ferramenta web e da aplicação PWA para visualização gráfica dos dados coletados, com etapas iterativas de desenvolvimento e refinamento baseadas na metodologia incremental.


.. image:: img/victusexergame/Esquema_de_funcionamento_VictusExergame.png
.. image:: img/victusexergame/waypointsPista2.png
.. image:: img/victusexergame/waypointsPista1.png
.. image:: img/victusexergame/Pista2.png
.. image:: img/victusexergame/Pista1.png
.. image:: img/victusexergame/menuPrincipalExergame.png
.. image:: img/victusexergame/menuConfiguracoes.png
.. image:: img/victusexergame/CapturaVictusExergame.png
.. image:: img/victusexergame/luis.png
.. image:: img/victusexergame/glenio.png


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png


