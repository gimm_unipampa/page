.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_

Physio Games
================
	
A fisioterapia, dedicada ao diagnóstico, prevenção e tratamento de doenças e lesões por métodos físicos, busca aliviar desconfortos e facilitar o retorno gradual dos pacientes à rotina. Para amputados, as sessões de fisioterapia muitas vezes são desafiadoras e pouco motivadoras. Este trabalho propõe um ambiente para a reabilitação física de amputados, utilizando um jogo sério desenvolvido em Godot. A solução, chamada Physio Games, inclui um nó sensor com um módulo giroscópio e uma aplicação web. Baseado no protótipo Tennis Game Physio, o Physio Games visa coletar dados objetivos do paciente para tornar as avaliações menos subjetivas, contribuindo para resultados mais eficazes nas sessões de reabilitação. O sistema apresenta uma nova arquitetura de nó sensor, modificações no ambiente de \textit{software}, segregação entre o ambiente de simulação e a visualização de dados, uma base de dados, sistema de autenticação e um novo jogo. O desenvolvimento percorreu diversas etapas, desde o levantamento do referencial teórico até a fase de prototipação e aplicação de testes. Os resultados indicam que o sistema é de interesse dos fisioterapeutas e, estabelece uma base sólida para as próximas etapas do projeto.



.. image:: img/physiogames/arquitetura1.png
.. image:: img/physiogames/arquitetura2.png
.. image:: img/physiogames/tennisgame.png
.. image:: img/physiogames/nova_fase.png

.. image:: img/physiogames/arq_proposta.png
.. image:: img/physiogames/arq_final.png
.. image:: img/physiogames/selectgamepong.png
.. image:: img/physiogames/esquemadeligacaoESPmpu.png
.. image:: img/physiogames/Login instituição.png
.. image:: img/physiogames/InfoPacient.png
.. image:: img/physiogames/esqueciSenha.png
.. image:: img/physiogames/menuInst.png
.. image:: img/physiogames/cenajogo1.png
.. image:: img/physiogames/cenajogo2.png






Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png

