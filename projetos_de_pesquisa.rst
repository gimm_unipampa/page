.. image:: img/bannergimm.png

Projetos de Pesquisa
**************

2023 - 2026
============
MIPWise: Inteligência Artificial das Coisas Aplicada ao Manejo Integrado de Pragas
Descriçao: Um dos desaﬁos da agricultura, os fatores bióticos que limitam a produção de alimentos, é a ocorrência de pragas.Estima-se que os prejuízos pelo ataque de pragas na agricultura brasileira chegam a R$55 bilhões por ano (Embrapa),representando quase 10% da produção agrícola nacional.Com vistas a uma demanda crescente por alimentos e a sustentabilidade, torna-se imprescindível a adoção de tecnologias que visam a melhoria na eﬁciência do controle de pragas, baseadas nos preceitos do Manejo Integrado de Pragas (MIP).O objetivo é estabelecer uma área para testes e validação de tecnologias da agricultura 4.0, tendo como estudo de caso o aprimoramento e avaliação de um sistema integrado de sensores para coleta de dados e tomada de decisão para manejo integrado de pragas de uma startup de base tecnológica.
Situação: Em andamento; Natureza: Pesquisa.
Financiador(es): Fundação de Amparo à Pesquisa do Estado do Rio Grande do Sul - Auxílio financeiro.


2023 - 2026
============
Construção, automatização e operação de equipamento para o monitoramento de variáveis meteorológicas e de partículas respiráveis e inaláveis suspensas na atmosfera

Descrição: Construção, automatização e operação de equipamento para o monitoramento de variáveis meteorológicas e de partículas respiráveis e inaláveis suspensas na atmosfera.
A poluição atmosférica apesar de não ser considerada uma temática nova, deve ser constantemente investigada, uma vez que cerca de 7 milhões de pessoas morrem anualmente por suas consequências. Além disso, quase toda a população do mundo (99%) respira ar que excede os limites de qualidade recomendados pelas Diretrizes Globais de Qualidade do Ar, estabelecidas pela Organização Mundial de Saúde (OMS). Nesse contexto, apesar de possuir um aparato legislativo e também um considerável Sistema Nacional de Meio Ambiente (SISNAMA), o Brasil ainda apresenta deﬁciências no âmbito da poluição atmosférica, pois muitos municípios do país não realizam monitoramento, diﬁcultando os processos de controle. O estado do Rio Grande do Sul, por exemplo, possui 497 municípios e destes, apenas 5 (Canoas, Esteio, Gravataí, Guaíba e Triunfo) monitoram a poluição atmosférica. O presente Projeto visa contribuir com a rede de monitoramento do Rio Grande do Sul, construindo um equipamento mais acessível ﬁnanceiramente, porém com a eﬁciência dos equipamentos que já são consolidados no mercado. Bagé seria o primeiro município monitorado na Região da Campanha Gaúcha, com pretensão de expandir para os demais municípios futuramente.
Situação: Em andamento; Natureza: Pesquisa.
Alunos envolvidos: Graduação: (3) .
Financiador(es): Fundação de Amparo à Pesquisa do Estado do Rio Grande do Sul - Bolsa.




2021-2024
=========

Desenvolvimento de Soluções Tecnológicas Baseadas em Sistemas Embarcados e Microeletrônica Aplicadas a Região da Fronteira Sul do Rio Grande do Sul.

Descrição: A área de microeletrônica, em especial a de sistemas embarcados têm sido amplamente explorada nas últimas décadas, com o objetivo de prover soluções computacionais que agreguem valor nas mais diferentes áreas. Dentre as diferentes potencialidades da área de sistemas embarcados, existem duas temáticas em destaque nos últimos anos, e que são o foco deste projeto, são elas: Agricultura de Precisão com apoio de sistemas IOT (Internet of Things) e Informática Médica. Neste contexto, vale ressaltar que a Informática Médica é uma das áreas que vem se destacando no cenário da computação com diversos avanços tecnológicos para a área da saúde, desempenhando um papel muito importante na coleta, análise, processamento e armazenamento de dados. Assim, considerando esses pontos elencados acima, é inegável a importância do desenvolvimento de tecnologias com enfoque na área médica. A primeira linha de pesquisa e desenvolvimento desse projeto visa corroborar com a evolução da área de informática médica no processo de reabilitação física de pacientes através de soluções tecnológicas baseada em sistemas embarcados. Por outro lado, os fatores locais e o perfil socioeconômico da região da região da campanha, também se tornam importantes ao vislumbrar alternativas de produção de conhecimento e soluções tecnológicas para promover o desenvolvimento da região, como por exemplo, a proposta de soluções que auxiliem a cadeia produtiva agropecuária regional através da expertise dos cursos de computação da Unipampa. Neste sentido, outra demanda crescente no Brasil, é a busca por soluções que auxiliem a produção do agronegócio nas mais diferentes tarefas, seja na tomada de decisão ou no desenvolvimento de sistemas computacionais que automatizem etapas. Nesta temática, diversos tópicos ganham destaque, o principal deles é a Agricultura de Precisão. O presente projeto também pretende estender linhas de pesquisa e desenvolvimento utilizando sistemas embarcados na área de agricultura de precisão, Smart farms e agricultura digital (AD), visando corroborar com o perfil intelectual da Unipampa e seus cursos de computação, bem como com a região da campanha e sua aptidão natural para a área do agronegócio..
Situação: Em andamento; Natureza: Pesquisa.
Alunos envolvidos: Graduação: (2).

CAAE: 51777521.5.0000.5323

2016 - 2018
===========

Uso da computação como instrumento de apoio na reabilitação física de pacientes amputados 

Descrição: Este projeto vislumbra estudar e propor soluções computacionais para servir como instrumento de apoio a profissionais da área de saúde, no processo reabilitação física de pacientes amputados, utilizando tecnologias como Arduíno, sensores, a linguagem C++ e o motor de jogo 3D Unity. Objetiva-se estimular o paciente durante as atividades físicas, além de fornecer dados pontuais para o ajuste no tratamento proposto, a fim de reabilitar indivíduos que sofreram amputação de membros inferiores ou superiores..
Situação: Concluído; Natureza: Pesquisa.
Alunos envolvidos: Graduação: (6) .

CAAE: 60663016.5.0000.5323


Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_

Apoio:
=====
.. image:: img/rodape1.png
