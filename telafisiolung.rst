.. image:: img/bannergimm.png

`Voltar <projetos.html#>`_


FisioLung
===========


A fisioterapia respiratória é necessária para o tratamento de pacientes acometidos por doenças pulmonares. Nas sessões são aplicadas técnicas para a remoção de secreções das vias aéreas. A técnica de vibração torácica manual é extremamente importante para a higienização brônquica, em razão da dificuldade de execução da técnica, construiu-se uma ferramenta para análise de sua execução. Com um sistema de monitoramento a partir do registro de sessões, torna-se possível o acompanhamento da evolução do paciente e da aplicação da técnica de vibração. O sistema FisioLung é capaz de registrar as sessões de fisioterapia com o cadastro de dados pessoais do paciente e do profissional, são armazenadas as informações de anamnese, técnicas utilizadas e análise da técnica de vibração torácica manual. O FisioLung é baseado na integração de hardware e software, é utilizado um acelerômetro para captar os dados de vibração e uma placa Arduino para processá-los, o software da aplicação utiliza a arquitetura Model-View-Controller para integração entre o front-end e back-end. As análises de sessões tornam-se importantes para acompanhar a evolução do paciente ao decorrer do tratamento. Na tela de análise são exibidos os gráficos de picos de frequência versus tempo e da média dos picos de frequência versus tempo, além de informações das sessões como: tempo de análise, dados de anamnese e técnicas utilizadas. A modelagem foi realizada a partir dos requisitos definidos, prototipação de telas, casos de uso, digramas de classe e sequência, e banco de dados. De acordo com os testes funcionais realizados no sistema foi possível mensurar o desempenho, executando o esperado para a solução. O desenvolvimento do FisioLung permitiu uma melhora de performance da execução da técnica de vibração torácica, beneficiando os pacientes durante o tratamento. As análises das sessões permitiram um melhor acompanhamento do paciente pelos fisioterapeutas, identificando possíveis melhorias e recaídas.


.. image:: img/fisiolung/fl.png
.. image:: img/fisiolung/fl1.png
.. image:: img/fisiolung/fl2.png
.. image:: img/fisiolung/fl3.png

FisioLung 2.0
========
Fisiolung 2.0 propõe melhorias para o sistema desenvolvido integrando hardware e software. A nova arquitetura conta com uma luva com acelerômetro para capturar os dados de vibração e uma placa NodeMCU ESP32 utilizada para processá-los. Os dados recolhidos são enviados para a aplicação móvel flutter que gere a sessão. O sistema pode gravar as sessões de fisioterapia com o registro dos dados do paciente e do profissional, informações da anamnese, técnicas utilizadas e análise da técnica de vibração manual do tórax armazenada. Na tela de análise é possível observar os gráficos de pico de frequência versus tempo e a média de pico de frequência versus tempo, além de informações da sessão como tempo de análise, dados da anamnese e técnicas aplicáveis. Alguns experimentos foram realizados e os resultados iniciais mostraram que o sistema poderia beneficiar os fisioterapeutas na aplicação da técnica de Vibrocompressão.

.. image:: img/fisiolung/arquitetura2.png
.. image:: img/fisiolung/smartphone.jpg
.. image:: img/fisiolung/freq.png
.. image:: img/fisiolung/experimento.jpg
.. image:: img/fisiolung/sinaisfinais.png



Informações:
==================
* `Principal <index.html#>`_
* `Equipe <equipe.html#>`_
* `Projetos <projetos.html#>`_
* `Projetos de Pesquisa <projetos_de_pesquisa.html#>`_
* `Publicações <publicacoes.html#>`_
* `Trabalhos de Conclusão de Curso <trabalhos_conclusao_curso.html#>`_
* `Parcerias <parcerias.html#>`_
Apoio:
=====
.. image:: img/rodape1.png
